#include <iostream>
#include "sha256.h"

using std::string;
using std::cout;
using std::cin;
using std::endl;

void calculate_hash();

int main(int argc, char *argv[]){
    //limite arg to 2 values
    if (argc > 2){
        cout << "ERROR! Usage: arg + value - no more then 2 values" << endl;
    }
    calculate_hash();

}

void calculate_hash(){
    string input;
    cin >> input;
    cout << sha256(input) <<endl;
}